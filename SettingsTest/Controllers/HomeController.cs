﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SettingsTest.Models;

namespace SettingsTest.Controllers
{
    public class HomeController : Controller
    {
        private string setting1 { get; set; }
        private string setting2 { get; set; }
        public HomeController(IConfiguration configuration)
        {
            setting1 = configuration["AppSettings:Setting1"];
            setting2 = configuration["AppSettings:Setting2"];
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = $"Setting1: {setting1} and Setting2: {setting2}";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
